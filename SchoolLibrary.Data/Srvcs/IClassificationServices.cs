﻿using SchoolLibrary.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolLibrary.Data.Srvcs
{
    public interface IClassificationServices
    {
        Task<List<ClassificationDto>> GetAllBooks();

        Task<ClassificationDto> AddBook(ClassificationDto dto);

        Task<ClassificationDto> GetBook(int signId);

        Task<ClassificationDto> UpdateBook(ClassificationDto dto);

        Task<bool> DeleteBook(int signId);
    }
}
