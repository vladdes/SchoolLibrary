﻿using SchoolLibrary.Data.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SchoolLibrary.Data.Srvcs
{
    public interface IAuthorServices
    {
        /// <summary>
        /// Gets all Authors
        /// </summary>
        /// <returns>List-AuthorDto</returns>
        Task<List<AuthorDto>> GetAllAuthors();
        /// <summary>
        /// Add a single Author
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>AuthorDto</returns>
        Task<AuthorDto> AddAuthor(AuthorDto dto);
        /// <summary>
        /// Get a single Author
        /// </summary>
        /// <param name="aid"></param>
        /// <returns>AuthorDto</returns>
        Task<AuthorDto> GetAuthor(int aid);
        /// <summary>
        /// Pagination authors
        /// </summary>
        /// <param name="skip"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task<List<AuthorDto>> GetAuthorsFrom(int skip, int amount);
        /// <summary>
        /// Update an Author
        /// </summary>
        /// <param name="dto"></param>
        /// <returns>AuthorDto</returns>
        Task<AuthorDto> UpdateAuthor(AuthorDto dto);
        /// <summary>
        /// Removes an author
        /// </summary>
        /// <param name="aids"></param>
        /// <returns></returns>
        Task<bool> DeleteAuthor(int aid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ISBN"></param>
        /// <returns></returns>
        Task<List<AuthorDto>> GetAuthorsForBook(string ISBN);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtos"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        Task<bool> AddBooksToAuthor(List<BookDto> dtos, int aid);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="aid"></param>
        /// <returns></returns>
        Task<bool> RemoveBookFromAuthor(string ISBN, int aid);

    }
}
