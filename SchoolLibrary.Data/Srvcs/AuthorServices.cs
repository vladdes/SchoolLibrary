﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using SchoolLibrary.Data.Dto;
using SchoolLibrary.Models;

namespace SchoolLibrary.Data.Srvcs
{
    public class AuthorServices : IAuthorServices
    {
        private readonly ApplicationDbContext applicationDbContext;

        public AuthorServices(ApplicationDbContext applicationDbContext)
        {
            this.applicationDbContext = applicationDbContext;
        }

        public async Task<List<AuthorDto>> GetAllAuthors()
        {
            try
            {
                var authorsDb = await applicationDbContext.Authors.ToListAsync();
                if (authorsDb == null)
                    return null;

                List<AuthorDto> authorDtos = new List<AuthorDto>();

                authorDtos = (from a in authorsDb
                              orderby a.LastName
                              select new AuthorDto()
                              {
                                  Aid = a.Aid,
                                  FirstName = a.FirstName,
                                  LastName = a.LastName,
                                  BirthYear = a.BirthYear
                              }).ToList();
                return authorDtos;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<AuthorDto> GetAuthor(int aid)
        {
            try
            {
                var authorDto = await (from a in applicationDbContext.Authors
                                       where a.Aid == aid
                                       select new AuthorDto()
                                       {
                                           Aid = a.Aid,
                                           FirstName = a.FirstName,
                                           LastName = a.LastName,
                                           BirthYear = a.BirthYear
                                       }).FirstOrDefaultAsync();
                if (authorDto == null)
                    return null;

                return authorDto;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<AuthorDto> UpdateAuthor(AuthorDto dto)
        {
            try
            {
                var authorDb = await applicationDbContext.Authors.SingleOrDefaultAsync(x => x.Aid == dto.Aid);
                if (authorDb != null)
                    return null;


                authorDb.BirthYear = dto.BirthYear;
                authorDb.FirstName = dto.FirstName;
                authorDb.LastName = dto.LastName;

                var result = await applicationDbContext.SaveChangesAsync();

                if (result == 0)
                    return null;

                return dto;

            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<bool> DeleteAuthor(int aid)
        {
            try
            {
                var authorDb = await applicationDbContext.Authors.SingleOrDefaultAsync(x => x.Aid == aid);

                if (authorDb == null)
                    return false;


                var deletedAuthor = applicationDbContext.Authors.Remove(authorDb);

                if (deletedAuthor != null)
                {
                    var saved = await applicationDbContext.SaveChangesAsync();
                    return saved == 1 ? true : false;
                }

                return false;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<AuthorDto> AddAuthor(AuthorDto dto)
        {
            try
            {
                var author = new AUTHOR()
                {
                    Aid = dto.Aid,
                    BirthYear = dto.BirthYear,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName
                };
                var authorDb = applicationDbContext.Authors.Add(author);
                if (authorDb == null)
                    return null;

                var added = await applicationDbContext.SaveChangesAsync();

                return added == 1 ? dto : null;

            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<List<AuthorDto>> GetAuthorsFrom(int skip, int amount)
        {
            try
            {
                var authorsDb = await applicationDbContext.Authors.ToListAsync();
                if (authorsDb == null)
                    return null;
                var authors = authorsDb.Skip(skip).Take(amount);
                List<AuthorDto> authorDtos = new List<AuthorDto>();
                
                authorDtos = (from a in authors
                              orderby a.LastName
                              select new AuthorDto()
                              {
                                  Aid = a.Aid,
                                  FirstName = a.FirstName,
                                  LastName = a.LastName,
                                  BirthYear = a.BirthYear

                              }).ToList();
                return authorDtos;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<List<AuthorDto>> GetAuthorsForBook(string ISBN)
        {
            try
            {
                var bookAuthors = await applicationDbContext.BookAuthors.Where(x => x.ISBN == ISBN).ToListAsync();
                var authors = await applicationDbContext.Authors.Where(x => bookAuthors.Any(b => b.Aid == x.Aid)).ToListAsync();
                List<AuthorDto> authorDtos = new List<AuthorDto>();
                authorDtos = (from a in authors
                              orderby a.LastName
                              select new AuthorDto()
                              {
                                  Aid = a.Aid,
                                  FirstName = a.FirstName,
                                  LastName = a.LastName,
                                  BirthYear = a.BirthYear,


                              }).ToList();
                return authorDtos;

            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<bool> AddBooksToAuthor(List<BookDto> dtos, int aid)
        {
            try
            {
                List<BOOKAUTHOR> bookAuthors = new List<BOOKAUTHOR>();
                foreach (var bookDto in dtos)
                {
                    bookAuthors.Add(new BOOKAUTHOR()
                    {
                        Aid = aid,
                        ISBN = bookDto.ISBN

                    });
                }
                applicationDbContext.BookAuthors.AddRange(bookAuthors);
                var result = await applicationDbContext.SaveChangesAsync();
                if (result != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }

        public async Task<bool> RemoveBookFromAuthor(string ISBN, int aid)
        {

            try
            {
                var bookAuthor = await applicationDbContext.BookAuthors.FirstOrDefaultAsync(x => x.Aid == aid && x.ISBN == ISBN);
                if (bookAuthor == null)
                    return false;

                applicationDbContext.BookAuthors.Remove(bookAuthor);
                var result = await applicationDbContext.SaveChangesAsync();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }

        }
    }
}