﻿using SchoolLibrary.Data.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolLibrary.Data.Srvcs
{
    public interface IBookServices
    {
       
        Task<List<BookDto>> GetAllBooks();
        
        Task<BookDto> AddBook(BookDto dto);
        
        Task<BookDto> GetBook(string ISBN);
       
        Task<BookDto> UpdateBook(BookDto dto);
        
        Task<bool> DeleteBook(string ISBN);

        Task<List<BookDto>> GetBooksFrom(int skip, int amount);

        Task<List<BookDto>> GetBooksForAuthor(int aid);
    }
}
