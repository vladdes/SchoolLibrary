﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SchoolLibrary.Data.Dto;
using SchoolLibrary.Models;
using System.Data.Entity;

namespace SchoolLibrary.Data.Srvcs
{
    public class BookServices : IBookServices
    {
        private readonly ApplicationDbContext dbContext;

        public BookServices(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<BookDto> AddBook(BookDto dto)
        {
            try
            {
                BOOK bookDb = new BOOK()
                {
                    ISBN = dto.ISBN,
                    Pages = dto.Pages,
                    PublicationInfo = dto.PublicationInfo,
                    PublicationYear = dto.PublicationYear,
                    SignId = dto.SignId,
                    Title = dto.Title

                };

                dbContext.Books.Add(bookDb);
                var result = await dbContext.SaveChangesAsync();
                if (result == 0)
                    return null;

                return dto;
            } catch(Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            
        }

        public async Task<List<BookDto>> GetBooksForAuthor(int aid)
        {
            try
            {
                var bookAuthors = await dbContext.BookAuthors.Where(x => x.Aid == aid).ToListAsync();
                var books = await dbContext.Books.Where(x => bookAuthors.Any(b => b.ISBN == x.ISBN)).ToListAsync();
                List<BookDto> bookDtos = new List<BookDto>();
                bookDtos = (from a in books
                              orderby a.Title
                              select new BookDto()
                              {
                                  ISBN = a.ISBN,
                                  Pages = a.Pages,
                                  PublicationInfo = a.PublicationInfo,
                                  PublicationYear = a.PublicationYear,
                                  SignId = a.SignId,
                                  Title = a.Title

                              }).ToList();

                return bookDtos;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            
        }

        public async Task<bool> DeleteBook(string ISBN)
        {
            try
            {
                var bookDb = await dbContext.Books.SingleOrDefaultAsync(x => x.ISBN == ISBN);

                if (bookDb == null)
                    return false;


                var deletedAuthor = dbContext.Books.Remove(bookDb);

                if (deletedAuthor != null)
                {
                    var saved = await dbContext.SaveChangesAsync();
                    return saved == 1 ? true : false;
                }

                return false;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            
        }

        public async Task<List<BookDto>> GetAllBooks()
        {
            try
            {
                var booksDb = await dbContext.Books.ToListAsync();
                if (booksDb == null)
                    return null;

                List<BookDto> booksDtos = new List<BookDto>();

                booksDtos = (from b in booksDb
                              orderby b.Title
                              select new BookDto()
                              {
                                  ISBN = b.ISBN,
                                  Pages = b.Pages,
                                  PublicationInfo = b.PublicationInfo,
                                  PublicationYear = b.PublicationYear,
                                  SignId = b.SignId,
                                  Title = b.Title
                              }).ToList();

                return booksDtos;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
           
        }

        public async Task<BookDto> GetBook(string ISBN)
        {
            try
            {
                var bookDto = await (from b in dbContext.Books
                                       where b.ISBN == ISBN
                                       select new BookDto()
                                       {
                                           ISBN = b.ISBN,
                                           Pages = b.Pages,
                                           PublicationInfo = b.PublicationInfo,
                                           PublicationYear = b.PublicationYear,
                                           SignId = b.SignId,
                                           Title = b.Title
                                       }).FirstOrDefaultAsync();
                if (bookDto == null)
                    return null;

                return bookDto;
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            
        }

        public async Task<List<BookDto>> GetBooksFrom(int skip, int amount)
        {
            try
            {
                var booksDb = await dbContext.Books.ToListAsync();
                if (booksDb == null)
                    return null;
                var booksSkipped = booksDb.Skip(skip).Take(amount);
                List<BookDto> booksDtos = new List<BookDto>();

                booksDtos = (from b in booksSkipped
                             orderby b.Title
                             select new BookDto()
                             {
                                 ISBN = b.ISBN,
                                 Pages = b.Pages,
                                 PublicationInfo = b.PublicationInfo,
                                 PublicationYear = b.PublicationYear,
                                 SignId = b.SignId,
                                 Title = b.Title
                             }).ToList();

                return booksDtos;
                
            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            
        }

        public async Task<BookDto> UpdateBook(BookDto dto)
        {
            try
            {

            }
            catch (Exception exc)
            {
                throw new Exception(exc.Message, exc.InnerException);
            }
            throw new NotImplementedException();
        }
    }
}