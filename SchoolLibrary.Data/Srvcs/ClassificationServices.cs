﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SchoolLibrary.Data.Dto;

namespace SchoolLibrary.Data.Srvcs
{
    public class ClassificationServices : IClassificationServices
    {
        private readonly ApplicationDbContext dbContext;

        public ClassificationServices(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Task<ClassificationDto> AddBook(ClassificationDto dto)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteBook(int signId)
        {
            throw new NotImplementedException();
        }

        public Task<List<ClassificationDto>> GetAllBooks()
        {
            throw new NotImplementedException();
        }

        public Task<ClassificationDto> GetBook(int signId)
        {
            throw new NotImplementedException();
        }

        public Task<ClassificationDto> UpdateBook(ClassificationDto dto)
        {
            throw new NotImplementedException();
        }
    }
}