﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolLibrary.Data.Dto
{
    public class AuthorDto
    {
        public int Aid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthYear { get; set; }

        public List<BookDto> Books { get; set; }

    }
}