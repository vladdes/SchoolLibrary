﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolLibrary.Data.Dto
{
    public class BookDto
    {

        public string ISBN { get; set; }
        
        public string Title { get; set; }
       
        public string SignId { get; set; }

        public DateTime PublicationYear { get; set; }

        public string PublicationInfo { get; set; }

        public int Pages { get; set; }

        public List<AuthorDto> Authors { get; set; }

    }
}