﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolLibrary.Data.Dto
{
    public class ClassificationDto
    {
        public int SignId { get; set; }
        public string Signum { get; set; }
        public string Description { get; set; }
    }
}