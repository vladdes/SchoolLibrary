﻿using Microsoft.AspNet.Identity.EntityFramework;
using SchoolLibrary.Identity.Models;
using SchoolLibrary.Models;
using System.Data.Entity;

namespace SchoolLibrary.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

        }

        public DbSet<AUTHOR> Authors { get; set; }
        public DbSet<BOOK> Books { get; set; }
        public DbSet<CLASSIFICATION> Classifications { get; set; }
        public DbSet<BOOKAUTHOR> BookAuthors { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BOOKAUTHOR>()
                .HasKey(x => new { x.ISBN, x.Aid });
            Database.SetInitializer<ApplicationDbContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}