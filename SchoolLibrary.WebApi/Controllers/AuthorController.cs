﻿using SchoolLibrary.Data;
using SchoolLibrary.Data.Dto;
using SchoolLibrary.Data.Srvcs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace SchoolLibrary.WebApi.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/authors")]
    public class AuthorController : ApiController
    {
        private readonly AuthorServices authorServices;

        public AuthorController()
        {
            authorServices = new AuthorServices(new ApplicationDbContext());
        }

        // GET: Author
        [HttpGet]
        [Route("")]
        public async Task<List<AuthorDto>> GetAuthors()
        {
            
            
            return await authorServices.GetAllAuthors();
        }
    }
}