﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SchoolLibrary.Models
{
    public class AUTHOR
    {
        public AUTHOR()
        {
           
        }
        [Key]
        public int Aid { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthYear { get; set; }

        
    }
}
