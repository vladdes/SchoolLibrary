﻿using System.ComponentModel.DataAnnotations;


namespace SchoolLibrary.Models
{
    public class CLASSIFICATION
    {
        [Key]
        public int SignId { get; set; }
        public string Signum { get; set; }
        public string Description { get; set; }
    }
}
