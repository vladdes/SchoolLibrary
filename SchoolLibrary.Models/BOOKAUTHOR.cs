﻿using System;
using System.Collections.Generic;

using System.Text;

namespace SchoolLibrary.Models
{
    public class BOOKAUTHOR
    {
        
        public string ISBN { get; set; }
        public int Aid { get; set; }
        public byte Order { get; set; }

        //-----------------------------
        //Relationships
        
        public BOOK Book { get; set; }
        public AUTHOR Author { get; set; }
    }
}
