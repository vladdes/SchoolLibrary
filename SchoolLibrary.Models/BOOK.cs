﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SchoolLibrary.Models
{
    public class BOOK
    {
        public BOOK()
        {
            
        }

        [Key, Required]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ISBN { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string SignId { get; set; }

        public DateTime PublicationYear { get; set; }

        public string PublicationInfo { get; set; }

        public int Pages { get; set; }

        
    }
}
