﻿using SchoolLibrary.Data.Dto;
using SchoolLibrary.Data.Srvcs;
using SchoolLibrary.Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SchoolLibrary.Controllers
{
   
    public class HomeController : Controller
    {
        private readonly IAuthorServices _authorServices;

        public HomeController(IAuthorServices authorServices)
        {
            _authorServices = authorServices;
        }
        
        public async Task<ActionResult> Index()
        {

            var authors = await _authorServices.GetAllAuthors();
            return View();
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}