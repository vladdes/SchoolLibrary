﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Microsoft.Owin.Builder;
using Owin;
using SchoolLibrary.Data;
using SchoolLibrary.Data.Srvcs;
using SchoolLibrary.Extensions;
using System;
using System.Linq;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(SchoolLibrary.Startup))]
namespace SchoolLibrary
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var services = new ServiceCollection();
            ConfigureAuth(app);
            ConfigureServices(services);
            var resolver = new DefaultDependencyResolver(services.BuildServiceProvider());
            DependencyResolver.SetResolver(resolver);

        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ApplicationDbContext>();
            services.AddTransient<IAuthorServices, AuthorServices>();
            services.AddTransient<IBookServices, BookServices>();
            services.AddTransient<IClassificationServices, ClassificationServices>();
            
            services.AddControllersAsServices(typeof(Startup).Assembly.GetExportedTypes()
                                .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
                                .Where(t => typeof(IController).IsAssignableFrom(t)
                                || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));
            
        }

    }
}
