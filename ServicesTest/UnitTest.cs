﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SchoolLibrary.Data;
using SchoolLibrary.Data.Dto;
using SchoolLibrary.Data.Srvcs;
using SchoolLibrary.Models;

namespace ServicesTest
{
    [TestClass]
    public class UnitTest 
    {
        private readonly ApplicationDbContext context;
        public UnitTest()
        {
            context = new ApplicationDbContext();
        }
        [TestMethod]
        public async Task AddDeleteAuthor()
        {
            var author = new AuthorDto() {
                BirthYear = DateTime.Now.AddYears(-30),
                FirstName = "SomethingSomethingTest",
                LastName = "SomethingSomething"

            };
            TestAuthorServices service = new TestAuthorServices(context);
            var result = await service.AddAuthor(author);
            Assert.AreEqual(author.FirstName, result.FirstName);
            var resultDelete = await service.DeleteAuthor(result.Aid);
            Assert.IsTrue(resultDelete);

        }

        [TestMethod]
        public async Task GetAllAuthors()
        {
           
            TestAuthorServices service = new TestAuthorServices(context);
            var result = await service.GetAllAuthors();
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public async Task GetUpdateAuthor()
        {

            TestAuthorServices service = new TestAuthorServices(context);
            var allAuthors = await service.GetAllAuthors();
            var updatedDto = allAuthors[0];
            updatedDto.FirstName = "Inte Bajs";
            var result = await service.UpdateAuthor(updatedDto);

            Assert.AreEqual(updatedDto.FirstName, result.FirstName);

        }

        public class TestAuthorServices : IAuthorServices
        {
            private readonly ApplicationDbContext dbContext;
            public TestAuthorServices(ApplicationDbContext context)
            {
                dbContext = context;
            }
            public async Task<AuthorDto> AddAuthor(AuthorDto dto)
            {
                var author = new AUTHOR()
                {      
                    BirthYear = dto.BirthYear,
                    FirstName = dto.FirstName,
                    LastName = dto.LastName
                };
                var authorDb = dbContext.Authors.Add(author);
                if (authorDb == null)
                    return null;

                var added = await dbContext.SaveChangesAsync();
                var a = await dbContext.Authors.FirstOrDefaultAsync(au => au.Aid == authorDb.Aid);
                var authorDto = new AuthorDto()
                {
                    Aid = a.Aid,
                    BirthYear = a.BirthYear,
                    FirstName = a.FirstName,
                    LastName = a.LastName
                };

                return added == 1 ? authorDto : null;
            }

            public Task<bool> AddBooksToAuthor(List<BookDto> dtos, int aid)
            {
                throw new NotImplementedException();
            }

            public async Task<bool> DeleteAuthor(int aid)
            {
                var author = await dbContext.Authors.FirstOrDefaultAsync(x => x.Aid == aid);
                if (author == null)
                    return false;

                var removeResult = dbContext.Authors.Remove(author);
                var result = await dbContext.SaveChangesAsync();
                return result == 1 ? true : false;
            }

            public async Task<List<AuthorDto>> GetAllAuthors()
            {
                var authorsDb = await dbContext.Authors.ToListAsync();
                if (authorsDb == null)
                    return null;

                List<AuthorDto> authorDtos = new List<AuthorDto>();

                authorDtos = (from a in authorsDb
                              orderby a.LastName
                              select new AuthorDto()
                              {
                                  Aid = a.Aid,
                                  FirstName = a.FirstName,
                                  LastName = a.LastName,
                                  BirthYear = a.BirthYear
                              }).ToList();
                return authorDtos;

            }

            public async Task<AuthorDto> GetAuthor(int aid)
            {
                var authorDb = await dbContext.Authors.FirstOrDefaultAsync(x => x.Aid == aid);
                if (authorDb == null)
                    return null;
                var author = new AuthorDto()
                {
                    Aid = authorDb.Aid,
                    BirthYear = authorDb.BirthYear,
                    FirstName = authorDb.FirstName,
                    LastName = authorDb.LastName
                };
                return author;
            }

            public Task<List<AuthorDto>> GetAuthorsForBook(string ISBN)
            {
                throw new NotImplementedException();
            }

            public Task<List<AuthorDto>> GetAuthorsFrom(int skip, int amount)
            {
                throw new NotImplementedException();
            }

            public Task<bool> RemoveBookFromAuthor(string ISBN, int aid)
            {
                throw new NotImplementedException();
            }

            public async Task<AuthorDto> UpdateAuthor(AuthorDto dto)
            {
                var authorDb = await dbContext.Authors.FirstOrDefaultAsync(x => x.Aid == dto.Aid);
                if (authorDb == null)
                    return null;
                authorDb.BirthYear = dto.BirthYear;
                authorDb.FirstName = dto.FirstName;
                authorDb.LastName = dto.LastName;
                var saved = await dbContext.SaveChangesAsync();

                var aDb = await dbContext.Authors.FirstOrDefaultAsync(x => x.Aid == dto.Aid);
                if (aDb == null)
                    return null;

                var authorDto = new AuthorDto()
                {
                    Aid = aDb.Aid,
                    BirthYear = aDb.BirthYear,
                    FirstName = aDb.FirstName,
                    LastName = aDb.LastName
                };

                return authorDto;
            }
        }
    }
}
