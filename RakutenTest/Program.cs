﻿using System;
using System.Linq;


namespace RakutenTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var solu = new SolutionRectiLinear();

            Console.WriteLine(solu.solution(-4, 1, 2, 6, 0, -1, 4, 3));
            Console.WriteLine(solu.solution(-8, 1, 0, 6, 0, -1, 4, 3));
            Console.WriteLine(solu.solution(-3, 1, -1, 6, 0, -1, 4, 3));
            Console.WriteLine(solu.solution(-2, 1, -2, 6, 0, -1, 4, 3));
            Console.ReadLine();
        }

        class Solution
        {
            public int solution(int[] A)
            {
                // write your code in C# 6.0 with .NET 4.5 (Mono)
                var newArray = Array.FindAll(A, x => x > 0).Distinct().ToArray();
                if (newArray == null || newArray.Length == 0)
                    return 1;
                Array.Sort(newArray);

                for(var i = 1; i <= newArray.Length; i++)
                {
                    if (newArray[i - 1] != i)
                        return i;

                }
                return newArray.Length + 1;

            }
        }

        class SolutionRectiLinear
        {
            public int solution(int K, int L, int M, int N, int P, int Q, int R, int S)
            {
                // write your code in C# 6.0 with .NET 4.5 (Mono)
                var p1LowerLeft = new Point(K, L);
                var p1UpperRight = new Point(M,N);
                var p2LowerLeft = new Point(P, Q);
                var p2UpperRight = new Point(R, S);

                var p1Sum = Math.Abs(K - M) * Math.Abs(L - N);
                var p2Sum = Math.Abs(P - R) * Math.Abs(Q - S);

                if (IsOverlap(p1LowerLeft, p1UpperRight, p2LowerLeft, p1UpperRight))
                {
                    var xDistance = Math.Min(p1UpperRight.x, p2UpperRight.x) - Math.Max(p1LowerLeft.x, p2LowerLeft.x);
                    var yDistance = Math.Min(p1UpperRight.y, p2UpperRight.y) - Math.Max(p1LowerLeft.y, p2LowerLeft.y);

                    var p3Sum = xDistance * yDistance;
                    return (p1Sum + p2Sum - p3Sum);
                }

                return p1Sum + p2Sum;

            }

            public bool IsOverlap(Point p1ll, Point p1ur, Point p2ll, Point p2ur)
            {
                var xOutSide = p1ll.x > p2ll.x || p2ll.x > p1ur.x;
                if (xOutSide)
                    return true;

                var yOutSide = p1ll.y < p2ur.y || p2ll.y < p1ur.y;
                if (yOutSide)
                    return true;

                return false;
            }

        }

        class Point
        {
            public Int32 x { get; set; }
            public Int32 y { get; set; }

            public Point(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
        }

        


    }
}
